# **LATEST VERSION v0.0.8**

Table of Contents
=================
* [Table of Contents](#table-of-contents)
* [[WIP] Container with a collection of tools and some of my own dotfiles](#wip-container-with-a-collection-of-tools-and-some-of-my-own-dotfiles)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Todo](#todo)
* [<strong>Pipeline Configuration</strong>](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [<strong>Programming languages</strong>](#programming-languages)
* [Tools installed](#tools-installed)
   * [Docker](#docker)
   * [Network utils](#network-utils)
   * [Shell utils](#shell-utils)
   * [Dev utils](#dev-utils)
   * [Kubernetes](#kubernetes)
   * [Cloud tools](#cloud-tools)
   * [Custom dotfiles for :](#custom-dotfiles-for-)
   * [Tools used](#tools-used)
      * [Asdf plugins](#asdf-plugins)
      * [Asdf packages](#asdf-packages)
      * [Pip Packages](#pip-packages)
      * [Brew Packages](#brew-packages)
      * [Arkade Packages](#arkade-packages)
* [<strong>If you want to tweak this with your personal conf</strong>](#if-you-want-to-tweak-this-with-your-personal-conf)
* [<strong>Links of some of the tools used/installed</strong>](#links-of-some-of-the-tools-usedinstalled)
* [Repo Tree](#repo-tree)
Created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc)
# [WIP] Container with a collection of tools and some of my own dotfiles
Used for running an isolated dev enviroment based on Ubuntu:22.04
Please note that due to the number of installed packages and lack of any optimization (WIP) this image is **larger than 2GB**
# How to run this on your machine
If you want to have DIND(docker in docker) enabled run 
```shell
docker run -v /var/run/docker.sock:/var/run/docker.sock -ti registry.gitlab.com/frankper/mdc:v0.0.8
```
**Just a word of caution: If your container gets access to docker.sock, it means it has more privileges over your docker daemon. So when used in real projects, understand the security risks, and use it**

For using the container without DIND
```shell
docker run -ti registry.gitlab.com/frankper/mdc:v0.0.8
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* Large image size (>2gb)
# Todo 
* Add tests
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration
* Reduce image size if possible 
* Reduce container image build times (currently >35 minutes)

# **Pipeline Configuration**
## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mdc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if pressent
## Github Actions
WIP (at the moment blocked due to image size limitations on the [free](https://github.com/features/packages#pricing) github registry)

# **Programming languages**
* Go Rust Python
# Tools installed 
## Docker
Using docker in docker 
## Network utils 
* dnsutils arp-scan sipcalc bmon ipmitool traceroute nmap
## Shell utils
* sudo cryptsetup fzf bat wget git curl zsh vim neovim ncdu exa bat fd duf tldr ranger starship tmux direnv asdf
## Dev utils
* python-pip base-devel git github-cli gitlab-cli 
## Kubernetes
* jq yq kubectl helm fluxctl kustomize skaffold telepresense arkade kubetail k9s eksctl clusterctl istioctl
## Cloud tools 
* aws-cli sops eksctl terraform pulumi tflint
## Custom dotfiles for :
* spacevim ohMyZsh tmux starship
## Tools used 
### Asdf plugins
* tmux argo rust golang helm awscli direnv yarn kubectl 1password-cli eksctl kustomize k9s docker-compose krew kubetail nodejs pulumi kind hugo boundary consul nomad packer sentinel serf terraform vault waypoint
### Asdf packages
* terraform pulumi tflint kubetail k9s 
### Pip Packages 
* gitup pipenv virtualenv
### Brew Packages 
* ccat ctop dive dua-cli gh git-quick-stats macchina whalebrew lazydocker dry speedtest gitlab-cli tilt
### Arkade Packages 
* helm kubectl istioctl doctl clusterctl faas-cli
# **If you want to tweak this with your personal conf**
* To change the default container username edit [here](https://gitlab.com/frankper/mdc/-/blob/main/Dockerfile#L22)
* Add more pip packages [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/setup_pip.sh)
* Add more system(apt) packages [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/apt_packages_to_install.txt)
* Add more asdf packages or plugins [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/setup_asdf.sh) and add global defaults [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/dotfiles/asdf/.tool-verions)
* Add Brew packages [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/Brewfile
* Add Arkade packages [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/setup_arkade.sh)
* Add Krew Plugins [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/install_krew.sh)
* Zsh config is [here](https://gitlab.com/frankper/mdc/-/tree/main/utils/dotfiles/zsh) 
* Starship config is [here](https://gitlab.com/frankper/mdc/-/tree/main/utils/dotfiles/starship)
* Tmux config is pulled from [here](https://github.com/frankperrakis/tmux-config)
* Spacevim config is [here](https://gitlab.com/frankper/mdc/-/tree/main/utils/dotfiles/vim/init.toml)
* To modify the terminal werlcome message read [here](https://github.com/Macchina-CLI/macchina/wiki/Customization)
* To change aliases set in ZSH do it [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/dotfiles/zsh/.zsh_profile_main)
* To add custom/manual plugins in OhMyZsh do it [here](https://gitlab.com/frankper/mdc/-/blob/main/utils/scripts/setup_omz_plugins.sh)
# **Links of some of the tools used/installed**
* [Docker](https://docs.docker.com/) 
* [Spacevim](spacevim.org/)
* [Starship](https://starship.rs)
* [Arcade](https://github.com/alexellis/arkade)
* [Homebrew](https://brew.sh/)
* [Oh My Zsh](https://ohmyz.sh/)
* [Krew Plugin Manager](https://krew.sigs.k8s.io/)
* [Telepresense](https://www.telepresence.io/)
* [Pip](https://pypi.org/project/pip/)
* [Skaffold](https://skaffold.dev/)
* [Helm](https://helm.sh/)
* [Asdf](https://github.com/asdf-vm/asdf)
* [Direnv](https://direnv.net/)
* [Macchina](https://github.com/Macchina-CLI/macchina)
* [Git Quick Stats](https://github.com/arzzen/git-quick-stats)
* [Lazydocker](https://github.com/jesseduffield/lazydocker)
* [How to run Docker In Docker Container](https://devopscube.com/run-docker-in-docker/)
# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
├── README.md
└── utils/
   ├── dotfiles/
   │  ├── asdf/
   │  │  └── .tool-versions
   │  ├── bash/
   │  │  ├── .bash_logout
   │  │  ├── .bash_profile
   │  │  └── .bashrc
   │  ├── starship/
   │  │  └── starship.toml
   │  ├── vim/
   │  │  └── init.toml
   │  └── zsh/
   │     ├── .zsh_history
   │     ├── .zsh_profile_linux
   │     ├── .zsh_profile_main
   │     └── .zshrc
   └── scripts/
      ├── apt_packages_to_install.txt
      ├── Brewfile
      ├── Brewfile.lock.json
      ├── install_arkade.sh
      ├── install_asdf.sh
      ├── install_fzf.sh
      ├── install_go.sh
      ├── install_homebrew.sh
      ├── install_krew.sh
      ├── install_kubectl.sh
      ├── install_omb.sh
      ├── install_omzsh.sh
      ├── install_rust.sh
      ├── install_spacevim.sh
      ├── install_starship.sh
      ├── install_tmux_samoskin.sh
      ├── setup_arkade.sh
      ├── setup_asdf.sh
      ├── setup_homebrew.sh
      ├── setup_krew.sh
      ├── setup_omz_plugins.sh
      ├── setup_pip.sh
      └── update_my_system.sh
```
