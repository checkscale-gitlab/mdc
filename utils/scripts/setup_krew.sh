#!/usr/bin/env bash
# tested in debian 10 
declare packagelist1=(
  ctx
  ns
  fuzzy
  ktop
  kubectl-topology
  kubectl-free
  kubecolor
  kubectl-watch
  kubectl-clogs
  kubectl-clogs
  get-all
  status
  pod-dive
  janitor
  cost
  doctor
  flame
  fleet
  graph
  konfig
)

function InstallKrewAddons() {
for i in ${!packagelist1[@]}; do 
    kubectl krew install ${packagelist1[i]}
done
}

source ~/.zshrc
kubectl krew update
InstallKrewAddons
